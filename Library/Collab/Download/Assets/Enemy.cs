﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    [Header("Velocidades")]
    public float speed = 1;

    //[Header("Movimiento")]
    private Transform target;
    private int wavepointIndex = 0;

    //Vida
    public int health = 10;
    //dinero que da
    public int moneytodrop = 50;

    private EnemySpawn papi;

    void Start()
    {
        //asigna su velocidad inicial (ya que puede ser modificada) y de target va al primer waypoint
        papi = this.transform.parent.GetComponent<EnemySpawn>();
        target = papi.oleadas[papi.oleadaActual].waypoints.points[0];
    }

    //moverse al waypoint y si está a X distancia cambiarlo al siguiente
    void Update()
    {
        //if (!GameManager.gameEnded)
        //{
            if (health <= 0)
            {
                Destroy(gameObject);
                //PlayerStats.Money += moneytodrop;
            }
            Vector3 dir = target.position - transform.position;
            transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

            //Si estás a X distancia ve al siguiente punto
            if (Vector3.Distance(transform.position, target.position) <= 0.1f)
            {
                GetNextWaypoint();
            }
        //}
    }

    //Avanzar al siguiente waypoint y si no existe terminar (destroy)
    void GetNextWaypoint()
    {
        if (wavepointIndex >= papi.oleadas[papi.oleadaActual].waypoints.points.Length - 1)
        {
            EndPath();
            return;
        }
        wavepointIndex++;
        target = papi.oleadas[papi.oleadaActual].waypoints.points[wavepointIndex];
        //WaveSpawner.EnemiesAlive--;
        
    }



    void EndPath()
    {
        //vidas del jugador --;
        //WaveSpawner.EnemiesAlive--;
        Destroy(gameObject);
        //Manager_Obj.GetComponent<PlayerStats>().HitLives();
    }

    internal void hit(int damage) {
		print("Me entra: " + damage);
	}
}
