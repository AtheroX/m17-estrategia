﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// Esconde los tiles que sean de colisiones pero mantiene sus colisiones
/// </summary>
public class HideTilemapColliderOnPlay : MonoBehaviour{

    private TilemapRenderer tilemapRenderer;

    void Start(){
        tilemapRenderer = GetComponent<TilemapRenderer>();
        tilemapRenderer.enabled = false;
    }
}
