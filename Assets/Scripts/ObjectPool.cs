﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool: MonoBehaviour{

	/// <summary>
	/// TAMBIÉN IGNORA
	/// </summary>


    public static ObjectPool instance;

    public Pool bulletPool;
    GameObject bulletPoolRoot;

    private void Start() {
        if (instance == null) instance = this;
        bulletPoolRoot = new GameObject("Bullet Pool");
        bulletPoolRoot.transform.parent = transform;
        bulletPool = bulletPoolRoot.AddComponent<Pool>();
        bulletPool.quant = 50;
        //NECESITAMOS PASARLE EL ITEM DE BALA
        //bulletPool.baseItem = poolGameObjects.TryGetValue("bullet");
    }
    
    public static bool HasComponent<C>(GameObject obj) {
        return obj.GetComponent<C>() != null;
    }
}
