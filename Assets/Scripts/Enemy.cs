﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour{

    [HideInInspector] public Vector3 offsetToShoot; //Offset para que la torreta no dispare al centro
    Action actionOnEndOfPath; // Es una función por lambda que le entra por EnemySpawner

    [Header("Propiedades")]
	public int health = 10;
	public int damage = 2;
	public float atackCooldown = 1f;
	bool onCooldown;
    bool unhiteable;
	public float speed = 1;
	public int spaceToFillOfChest = 1;
	public float rangeOfAtack = .5f;
    public int moneyToDrop = 50;

	[HideInInspector] public EnemySpawn whoSpawnedMe;
    [HideInInspector] public Transform target;
    [HideInInspector] public int roundWhereIsSpawned = 0;
    private int wavepointIndex = 0;

	Color originalColor;


    void Start(){
        // Guarda a quien le ha invocado y pone el primer punto al que debe moverse
        whoSpawnedMe = this.transform.parent.GetComponent<EnemySpawn>();
        target = whoSpawnedMe.oleadas[whoSpawnedMe.oleadaActual].waypoints.points[0];
		actionOnEndOfPath += () => {
			if (health <= 0)
				GameManager.instance.ModifyMoney(moneyToDrop);
			else
				GameManager.instance.ChestChange(spaceToFillOfChest);
		};

		originalColor = transform.GetChild(0).GetComponent<SpriteRenderer>().color;
		InvokeRepeating("CheckForAtack", .1f, .1f);
    }

    void Update(){
        // Si su vida es 0 o menor termina su ruta y se destruye
        if (health <= 0){
            EndPath();
        }

		// Pilla el objetivo y hace un vector y se mueve a este 
		// (más adelante será un rigidbody y pasando por la función isoRenderer.SetDirection()
		if (!GameManager.instance.playing) return;
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        // Si está cerca de su objetivo va al siguiente
        if (Vector3.Distance(transform.position, target.position) <= 0.1f){
            GetNextWaypoint();
        }
    }


	public void CheckForAtack() {
		Player p = FindObjectOfType<Player>();
		if (Vector2.Distance(p.gameObject.transform.position, transform.position) <= rangeOfAtack && !onCooldown) {
			p.Hit(damage);
			onCooldown = true;
			StartCoroutine(AtackCooldown());

		}

	}

	IEnumerator AtackCooldown() {
		yield return new WaitForSeconds(atackCooldown);
		onCooldown = false;
	}

	/// <summary>
	/// Va al siguiente punto, si no hay un siguiente termina el path y se destruye
	/// </summary>
	void GetNextWaypoint(){
        if (wavepointIndex >= whoSpawnedMe.oleadas[roundWhereIsSpawned].waypoints.points.Length - 1){
            EndPath();
            return;
        }
        wavepointIndex++;
        target = whoSpawnedMe.oleadas[roundWhereIsSpawned].waypoints.points[wavepointIndex]; 
    }

    /// <summary>
    /// Si tiene una función en actionOnEndOfPath la ejecuta y después se destruye
    /// </summary>
    void EndPath(){
        if (actionOnEndOfPath != null)
            actionOnEndOfPath();
		
        Destroy(gameObject);
    }

    /// <summary>
    /// Recibe daño
    /// </summary>
    /// <param name="damage">Parametro de daño hecho a este enemigo</param>
    public void Hit(int damage) {
        if (!unhiteable) {
            health -= damage;
			StartCoroutine(DamageEffectSequence(Color.red, 1));
			StartCoroutine(Unhiteable());
        }
	}

    IEnumerator Unhiteable() {
        unhiteable = true;
        yield return new WaitForSeconds(0.3f);
        unhiteable = false;
    }

    /// <summary>
    /// Función que ejecuta al morir
    /// </summary>
    /// <param name="a">Función a ejecutar</param>
    public void SuscribeOnDead(Action a) {
        actionOnEndOfPath = a;
	}

	/// <summary>
	/// Cambia el color del player al color especificado durante el tiempo especificado
	/// </summary>
	/// <param name="dmgColor">Color al que cambia</param>
	/// <param name="duration">Tiempo que dura la vuelta del color original</param>
	IEnumerator DamageEffectSequence(Color dmgColor, float duration) {
		SpriteRenderer sr = transform.GetChild(0).GetComponent<SpriteRenderer>();
		sr.color = dmgColor;
		for (float t = 0; t < 1.0f; t += Time.deltaTime / duration) {
			sr.color = Color.Lerp(dmgColor, originalColor, t);
			yield return null;
		}
		sr.color = originalColor;
	}

	public void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position+new Vector3(0,.25f,0), rangeOfAtack);
	}
}
