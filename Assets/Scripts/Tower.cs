﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour{

	[Header("Propiedades")]
	public string enemyTag = "Enemy";
	[Range(0.1f,4f)] public float range;
	public int damage;

	private Transform target;
	private Enemy targetEnemy;
	private Transform shootPoint;

	[Space]
	public GameObject bulletPref;
	[Tooltip("Tiempo para disparear")] public float shootCoolDown;
    public bool canShoot = true;

	public int towerPositionOnHud;

    void Start(){
		//Soft update
        InvokeRepeating("UpdateTarget", 0f, 0.1f);
        shootPoint = this.transform.GetChild(0);
    }

    /// <summary>
    /// Pilla todos los enemigos y si el más cercano está a rango lo targetea
    /// </summary>
    void UpdateTarget() {
        //Pilla todos los enemigos
        GameObject[] allEnemies = GameObject.FindGameObjectsWithTag(enemyTag);

        //Busca al más cercano
        float enemyDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in allEnemies) {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < enemyDistance) {
                enemyDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        //Guarda al enemigo más cerca y a rango, si no está a rango guarda nulls
        if (nearestEnemy != null && enemyDistance <= range) {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
        } else {
            target = null;
            targetEnemy = null;
        }

    }

    void Update() {
        //Si no hay target no hace nada, si hay target apunta a él
        if (target == null) return;
        LookToTarget();
    }

    private void LookToTarget() {
        //Vector hacia enemigo y rota tu shootPoint hacia él
		Vector3 diff = target.position - shootPoint.position;
		diff.Normalize();
		float rot = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
		shootPoint.rotation = Quaternion.Euler(0f, 0f, rot);

        //Si puede disparar, dispara
		if (canShoot) {
			StartCoroutine(Shoot());
		}

    }

    /// <summary>
    /// Crea la bala, le da un objetivo y hace el cooldown
    /// </summary>
	IEnumerator Shoot() {
		GameObject bala = (GameObject)Instantiate(bulletPref, shootPoint.position, shootPoint.rotation);
		TowerBullet bullet = bala.GetComponent<TowerBullet>();
		if (bullet != null) {
			bullet.Seek(target);
			bullet.damage = this.damage;
        }

        canShoot = false;
        yield return new WaitForSeconds(shootCoolDown);
        canShoot = true;
	}

    /// <summary>
    /// Muestra el rango en caso de estar seleccionado y ser mayor de 0
    /// </summary>
	private void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;
		if(range > 0)
		    Gizmos.DrawWireSphere(transform.position, range);
	}
}
