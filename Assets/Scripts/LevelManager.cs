﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Lee los niveles pasados y activa/desactiva a los que no puedas ir
/// </summary>
public class LevelManager : MonoBehaviour{

	public GameObject[] levels;
	public int levelsDone = 2;

	int wonLevels;

	public void Start() {
		wonLevels = PlayerPrefs.GetInt("wonLevels", 0);
		print(wonLevels);
		levels = new GameObject[transform.GetChild(1).childCount];
		for (int i = 0; i < levels.Length; i++) {
			levels[i] = transform.GetChild(1).GetChild(i).gameObject;
			if (i >= levelsDone || i > wonLevels) {
				levels[i].GetComponent<Button>().interactable = false;
			}
			print("AAA"+i+" "+(i>wonLevels));
		}
	}
}
