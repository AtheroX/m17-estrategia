﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour{

    public static BuildManager instance;

    //Clase para las torretas
	[System.Serializable]
	public class BuildItem {
		#if UNITY_EDITOR
			public string Name;
		#endif
		public GameObject tower;
		public int buyPrice;
		public int sellPrice;
		public bool selected = false;

	}

	public Transform canvasTowers;
	public BuildItem[] towers;
	public int towerSelectedToBuild = -1;
	GameObject towerSelectedToSell = null;
	public GameObject sellPanel;
    private GameObject selectedTowerOnSphere;
	[Range(0.1f,3f)] public float distanceToBuild = 3f;

	private bool showingBuildSphere = false, showingSellPanel = false;
    [HideInInspector] public bool currentClickIsBuilding = false;
	private GameObject buildSphere;

    [HideInInspector] public Vector2 mousePosition;
	

    public void Start() {
        //SINGLETON
        if (instance == null) instance = this;

        //Obtener esfera de construcción y deshabilitarla
        buildSphere = FindObjectOfType<Player>().transform.GetChild(0).gameObject;
		buildSphere.GetComponent<SpriteRenderer>().enabled = false;

        //Obtener torre semitransparente 
        selectedTowerOnSphere = transform.GetChild(0).GetChild(0).gameObject;
        selectedTowerOnSphere.GetComponent<SpriteRenderer>().color = Color.clear;

        //Obtener los botones para construir del canvas, deshabilitar los innecesarios y cambiarles los sprites
        for (int i = 0; i < canvasTowers.childCount; i++) {
            if (i > towers.Length - 1) 
                canvasTowers.GetChild(i).gameObject.SetActive(false);
            else {
                canvasTowers.GetChild(i).GetChild(1).GetComponent<Image>().sprite =
                    towers[i].tower.GetComponent<SpriteRenderer>().sprite;
				canvasTowers.GetChild(i).GetChild(2).GetComponent<TMPro.TextMeshProUGUI>().text = towers[i].buyPrice+"";
				towers[i].tower.GetComponent<Tower>().towerPositionOnHud = i;
			}
        }
	}

	void Update() {
        //Que solo selecionen torretas los botones que tienen torres
        #region towers 
        if (Input.GetKeyDown(KeyCode.Alpha1) && towers.Length >= 1) {
            towerSelectedToBuild = 0;
        } else if (Input.GetKeyDown(KeyCode.Alpha2) && towers.Length >= 2) {
            towerSelectedToBuild = 1;
        } else if (Input.GetKeyDown(KeyCode.Alpha3) && towers.Length >= 3) {
            towerSelectedToBuild = 2;
        } else if (Input.GetKeyDown(KeyCode.Alpha4) && towers.Length >= 4) {
            towerSelectedToBuild = 3;
        }
        #endregion

        /* Sistema que muestra la torreta transparente, la cambia de color dependiendo de donde esté
         * el ratón. También muestra la esfera de construcción */
        #region MOSTRAR TORRETA TRASPARENTE Y ESFERA
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (towerSelectedToBuild != -1) {
            //Mostrar esfera
            if (!showingBuildSphere) {
                buildSphere.GetComponent<SpriteRenderer>().enabled = true;
                buildSphere.transform.localScale = new Vector2(distanceToBuild * 10f / 1.25f, distanceToBuild * 10f / 1.25f + 0.25f);
                showingBuildSphere = true;
            }

            // Si no tiene ningún objeto que no permita construir debajo y está en el area de construcción
            if (NotOverAnotherTower() && OnBuildSphere() && CheckMoney()) {
                SpriteRenderer selectedTowerOnSphereSR = selectedTowerOnSphere.GetComponent<SpriteRenderer>();
                selectedTowerOnSphereSR.sprite = towers[towerSelectedToBuild].tower.GetComponent<SpriteRenderer>().sprite;
                selectedTowerOnSphere.GetComponent<SpriteRenderer>().color = new Color(.75f, .75f, 1f, .5f);
                selectedTowerOnSphere.transform.position = new Vector3(mousePosition.x, mousePosition.y, 0);

            } else {
                SpriteRenderer selectedTowerOnSphereSR = selectedTowerOnSphere.GetComponent<SpriteRenderer>();
                selectedTowerOnSphereSR.sprite = towers[towerSelectedToBuild].tower.GetComponent<SpriteRenderer>().sprite;
                selectedTowerOnSphere.GetComponent<SpriteRenderer>().color = new Color(1f, .25f, .25f, .5f);
                selectedTowerOnSphere.transform.position = new Vector3(mousePosition.x, mousePosition.y, 0);
            }

            //Al hacer click derecho desselecciona la torreta y vuelve transparente la torreta de guía
            if(Input.GetAxis("Fire2") != 0) {
                ClearOnBuildAction();
                towerSelectedToBuild = -1;
                selectedTowerOnSphere.GetComponent<SpriteRenderer>().color = Color.clear;
            }
        }
        #endregion

        // TODO Comprobar dinerito

        /* Si está dentro de la esfera de construcción y da click izquierdo, si no está encima de un
         * objeto que no permita construir construye, en caso contrario limpia la selección */
        if (OnBuildSphere()) {    
			if (Input.GetAxis("Fire1") != 0) {
				if (NotOverAnotherTower() && CheckMoney())
					BuildSuccess();
				else
					ClearOnBuildAction();

            }

        } else { //Si no está en la esfera de selección y sigue teniendo una torreta seleccionada la deselecciona
			if (towerSelectedToBuild != -1 && Input.GetAxis("Fire1") != 0) {
				ClearOnBuildAction();
            }

        }

        /* Al soltar el click, si estaba construyendo deja el modo construir 
         * Esto es para que se cambie de modo entre disparar y construir */
        if (Input.GetAxis("Fire1") == 0 && currentClickIsBuilding)
            currentClickIsBuilding = false;

		// Hacer click derecho sobre una torreta abre el panel de venta 
		if (Input.GetAxis("Fire2") != 0) {
			towerSelectedToSell = OverTower();
			if (towerSelectedToSell != null && !showingSellPanel) {
				showingSellPanel = true;
				sellPanel.SetActive(true);
				sellPanel.transform.position = towerSelectedToSell.transform.position - new Vector3(0, .5f,0);
				StartCoroutine(DisableSellPanel());
			}
		}

		//Si te alejas demasiado de la torreta que tiene menú de venta se deselecciona
		if (towerSelectedToSell != null) {
			if (Vector2.Distance(towerSelectedToSell.transform.position, Player.instance.transform.position) >= 4) {
				DontSell();
			}
		}
	}

	/// <summary>
	/// Desabilita el menú de venta tras 5 segundos sin tocarlo
	/// </summary>
	/// <returns></returns>
	IEnumerator DisableSellPanel() {
		yield return new WaitForSeconds(5f);
		if (showingSellPanel) 
			DontSell();
	}

	/// <summary>
	/// Comprueba si tienes suficiente dinero como para comprar la torreta seleccionada
	/// </summary>
	/// <returns></returns>
	private bool CheckMoney() {
		return GameManager.instance.money >= towers[towerSelectedToBuild].buyPrice;
	}

	/// <summary>
	/// Primero construye la torreta seleccionada después deselecciona
	/// </summary>
	private void BuildSuccess() {
		BuildTower();
		SpendMoney();
        ClearOnBuildAction();
    }

	/// <summary>
	/// Resta el dinero que vale la torreta al del jugador
	/// </summary>
	private void SpendMoney() {
		GameManager.instance.ModifyMoney(-towers[towerSelectedToBuild].buyPrice);
	}

	/// <summary>
	/// Deselecciona la torreta, esconde la esfera y la torreta de selección
	/// </summary>
	private void ClearOnBuildAction() {
        showingBuildSphere = false;
        currentClickIsBuilding = true;
        buildSphere.GetComponent<SpriteRenderer>().enabled = false;
        selectedTowerOnSphere.GetComponent<SpriteRenderer>().color = Color.clear;
        towerSelectedToBuild = -1;
    }

    /// <summary>
    /// Lanza un raycast por todos los colliders debajo del ratón y devuelve si puede construir
    /// </summary>
    /// <returns> si hay un enemigo lo ignora, si hay una torre, una decoración o el 
    /// personaje devulve un false, si no hay nada de eso devuelve true</returns>
    private bool NotOverAnotherTower() {
		RaycastHit2D[] hits = Physics2D.RaycastAll(mousePosition, new Vector2(0, 0), 0.01f);
		bool thereIsATower = false;
        
		foreach (RaycastHit2D hit in hits) {
			if (hit) {
                if (hit.collider.transform.GetComponent<Enemy>() || hit.collider.transform.GetComponent<ParticleSystem>())
                    continue;
				if (hit.collider.transform.parent.GetComponent<Tower>() || hit.collider.transform.parent.GetComponent<Player>()
                    || hit.collider.transform.parent.parent.CompareTag("Decorations") || hit.collider.transform.CompareTag("Waypoint")) {
					thereIsATower = true;
				}
			}
		}
		return !thereIsATower;
	}

	/// <summary>
	/// Devuelve la torreta que esté debajo del ratón
	/// </summary>
	/// <returns>Torreta que esté debajo del ratón</returns>
	private GameObject OverTower() {
		RaycastHit2D[] hits = Physics2D.RaycastAll(mousePosition, new Vector2(0, 0), 0.01f);

		foreach (RaycastHit2D hit in hits) {
			if (hit.collider.transform.parent.GetComponent<Tower>())
				return hit.collider.gameObject.transform.parent.gameObject;
		}
		return null;
	}
    /// <summary>
    /// Si tiene una torreta seleccionada y está a rango
    /// </summary>
    /// <returns>Devuelve true si está a rango, falso si no</returns>
	private bool OnBuildSphere() {
		if (towerSelectedToBuild != -1) {
			if (Vector2.Distance(mousePosition, Player.instance.transform.position) <= distanceToBuild) {
				return true;
			}
		}
		return false;
	}

    /// <summary>
    /// Construye una torreta en la posición del ratón
    /// </summary>
	private void BuildTower() {
		GameObject aux = (GameObject)Instantiate(
			towers[towerSelectedToBuild].tower,
			new Vector3(mousePosition.x,mousePosition.y,0),
			Quaternion.identity);
        aux.transform.parent = transform.GetChild(0);
        aux.GetComponent<SpriteRenderer>().color = Color.white;
    }

    /// <summary>
    /// Función para los botones. Selecciona la torreta en posición <strong>i</strong>
    /// </summary>
    /// <param name="i">Torreta a seleccionar</param>
    public void SelectTower(int i) {
        if (i < towers.Length) {
            towerSelectedToBuild = i;
        }
    }

	/// <summary>
	/// Cancela la venta de torreta
	/// </summary>
	public void DontSell() {
		showingSellPanel = false;
		towerSelectedToSell = null;
		sellPanel.SetActive(false);
	}

	/// <summary>
	/// Devuelve el dinero de venta al jugador, elimina la torreta y quita el menú
	/// </summary>
	public void Sell() {
		GameManager.instance.ModifyMoney(towers[towerSelectedToSell.GetComponent<Tower>().towerPositionOnHud].sellPrice);
		Destroy(towerSelectedToSell);
		DontSell();
	}

	/// <summary>
	/// Esfera de rango de contrucción
	/// </summary>
    public void OnDrawGizmos() {
		Gizmos.color = Color.yellow;
		if (Application.isPlaying && distanceToBuild > 0)
			Gizmos.DrawWireSphere(Player.instance.transform.position, distanceToBuild);
	}
}

