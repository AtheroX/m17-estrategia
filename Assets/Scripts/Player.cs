﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour{

	[Header("Parámetros principales")]
	public int maxHealth = 20;
	bool dead = false;
	public int actualHealth;
    public int atackDamage = 1;
    public float atackSpeed = 1;
    public float bulletSpeed = 1;

    [Space][Header("Ataque")]
	[HideInInspector] public static Player instance;
    bool clicking = false;
    public GameObject witchBullet;
    public Rigidbody2D firePoint;

    [Space][Header("Move")]
    public float movementSpeed = 1f;
    public float runningSpeed = 3f;

	public Transform respawn;
	public TextMeshProUGUI txtRespawnTime;

	Rigidbody2D rb;
    IsometricCharacterRenderer isoRenderer;

	void Start(){
		if (instance == null)
			instance = this;
		else {
			Instantiate(instance);
			Destroy(this.gameObject);
		}
		
		dead = false;
		txtRespawnTime.gameObject.SetActive(false);
        rb = GetComponent<Rigidbody2D>();
        isoRenderer = GetComponentInChildren<IsometricCharacterRenderer>();
		actualHealth = maxHealth;
    }

    void Update() {
        // Si hace click izquierdo mientras no hay torretras construyendose o construidas en este click, dispara y entra en CD
        if (Input.GetAxis("Fire1") != 0 && BuildManager.instance.towerSelectedToBuild == -1 &&
            !clicking && !BuildManager.instance.currentClickIsBuilding && !dead && GameManager.instance.playing) {
            clicking = true;
            StartCoroutine(Shoot());
        }

        //Como firepoint tiene un RB debemos moverlo a mano
        firePoint.position = transform.position;
    }

    void FixedUpdate(){
		if (!dead) {
			//Vector de movimiento en base a Axis
			Vector2 currentPos = rb.position;
			Vector2 inputVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
			inputVector = Vector2.ClampMagnitude(inputVector, 1);

			//Con shift se corre
			Vector2 movement;
			if (Input.GetKey(KeyCode.LeftShift))
				movement = inputVector * runningSpeed;
			else
				movement = inputVector * movementSpeed;
			Vector2 newPos = currentPos + movement * Time.fixedDeltaTime;

			//Mover y cambiar el sprite según ese vector de movimiento
			isoRenderer.SetDirection(movement);
			rb.MovePosition(newPos);

			//Girar el firepoint en base a la posición del ratón
			Vector3 diff = BuildManager.instance.mousePosition - rb.position;
			diff.Normalize();
			float rot = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg - 90;
			firePoint.rotation = rot;
		}
    }

    /// <summary>
    /// Dispara y hace un CD en base a la variable AtackSpeed
    /// </summary>
    IEnumerator Shoot() {
        GameObject actBullet = Instantiate(witchBullet, firePoint.transform.position + new Vector3(0, 0, 10),
            firePoint.transform.rotation);
        Rigidbody2D rb = actBullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.transform.up * bulletSpeed, ForceMode2D.Impulse);

        WitchBullet wb = actBullet.GetComponent<WitchBullet>();
        wb.SetDamage(atackDamage);

        //Pone un timer a la bala para que se destruya en 10 segundos por si no colisiona con nada
        Destroy(actBullet, 10f); 
        yield return new WaitForSeconds(atackSpeed);
        clicking = false;
	}

	/// <summary>
	/// Al ser golpeado le baja la vida y cambia la barra
	/// Si llega a 0 muere y empieza el CD de respawn
	/// </summary>
	public void Hit(int damage) {
		actualHealth -= damage;
		GameManager.instance.HealthChange(maxHealth, actualHealth);
		StartCoroutine(DamageEffectSequence(Color.red, 1));
		if (actualHealth <= 0 && !dead) {
			dead = true;
			transform.GetChild(4).gameObject.SetActive(false);
			txtRespawnTime.gameObject.SetActive(true);
			StartCoroutine(OnRespawning(5));
		}
	}

	/// <summary>
	/// Cooldown de respawnear, inicialmente entran 5 y va disminuyendo y llamandose a si misma
	/// Al llegar a 0 respawnea en el respawnpoint y se hace 0 de daño para actualizar la barra de vida
	/// </summary>
	/// <returns></returns>
	IEnumerator OnRespawning(int respawnTime) {
		txtRespawnTime.text = respawnTime + "";
		respawnTime--;
		yield return new WaitForSeconds(1f);
		if (respawnTime > 0) 
			StartCoroutine(OnRespawning(respawnTime));
		else {
			dead = false;
			actualHealth = maxHealth;
			Hit(0);
			transform.position = respawn.position;
			transform.GetChild(4).gameObject.SetActive(true);
			txtRespawnTime.gameObject.SetActive(false);
		}
	}

	/// <summary>
	/// Cambia el color del player al color especificado durante el tiempo especificado
	/// </summary>
	/// <param name="dmgColor">Color al que cambia</param>
	/// <param name="duration">Tiempo que dura la vuelta del color original</param>
	IEnumerator DamageEffectSequence(Color dmgColor, float duration) {
		SpriteRenderer sr = transform.GetChild(transform.childCount-1).GetChild(0).GetComponent<SpriteRenderer>();
		sr.color = dmgColor;

		for (float t = 0; t < 1.0f; t += Time.deltaTime / duration) {
			sr.color = Color.Lerp(dmgColor, Color.white, t);

			yield return null;
		}
		sr.color = Color.white;
	}
}
