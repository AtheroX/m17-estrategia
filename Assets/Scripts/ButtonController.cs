﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour{

	public void ChangeScene(int i) {
		SceneManager.LoadScene(i);
	}

	public void NextLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

	public void RetryLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void ExitGame() {
		Application.Quit();
	}

	public void RemoveData() {
		PlayerPrefs.DeleteAll();
	}

	public void RemoveData(string data) {
		PlayerPrefs.DeleteKey(data);
		PlayerPrefs.Save();
	}
}
