﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour{

    public Oleada[] oleadas;
    public float primerSpawn = 1f;
    public float tiempoEntreRondas = 5f;
    public float enemiesSpeed = 1; 
    public int oleadaActual = 0;
    private bool shouldSpawn = true;

    /// <summary>
    /// Esta clase contiene cada una de las oleadas con la lista de enemigos a instanciar,
    /// la lista de puntos para su path y cada cuanto deben aparecer
    /// </summary>
    [System.Serializable]
    public class Oleada {
        public float timeBetweenEnemies = 0.5f;
        [HideInInspector] public int enemiesInstantiated = 0;
        [HideInInspector] public int enemiesDead = 0;
        public Waypoints waypoints;
        public GameObject[] enemies;
    }

    void Start(){
        InvokeRepeating("Spawn", primerSpawn, .5f);
    }    

    void Spawn(){
        // Si el jugador sigue vivo, siguen habiendo enemigos por instanciar y ha acabado el cooldown, spawnea otro enemigo
        if (Player.instance.gameObject != null && oleadaActual < oleadas.Length && shouldSpawn && GameManager.instance.playing){ 
            shouldSpawn = false;
            StartCoroutine(SpawnEnemy());
        }
    }

    /// <summary>
    /// Spawnea un enemigo y en caso de que haya más se repite a si misma hasta que no hay más
    /// </summary>
    IEnumerator SpawnEnemy() { 
        Oleada o = oleadas[oleadaActual]; // Por Comodidad

        if (o.waypoints == null) yield return null; // Si no hay puntos que pare el spawn

        // Esperar el tiempo entre enemigos
        yield return new WaitForSeconds(o.timeBetweenEnemies);

        // Genera el enemigo que necesita y le pone sus datos
        if (o.enemies[o.enemiesInstantiated] != null) {
            Enemy auxEn = Instantiate(o.enemies[o.enemiesInstantiated],
                o.waypoints.transform.parent).GetComponent<Enemy>();
            auxEn.whoSpawnedMe = this;
            auxEn.target = o.waypoints.points[0].transform;
            auxEn.roundWhereIsSpawned = oleadaActual;
            //auxEn.speed = enemiesSpeed;
        
            // Le mete un lambda en una acción que se ejecuta al morir
            auxEn.SuscribeOnDead(() => {
                StartCoroutine(TimerForNextRound());
            });
        }
        //Si hay más enemigos llama repite la corrutina
        o.enemiesInstantiated++;
        if (o.enemiesInstantiated < o.enemies.Length)
            StartCoroutine(SpawnEnemy());
    }

    /// <summary>
    /// Función que va contando todos los muertos de una oleada y cuando hay la misma cantidad
    /// de muertos que de enemigos para spawnear permite paso a la siguiente oleada
    /// </summary>
    /// <returns></returns>
    IEnumerator TimerForNextRound() {
        oleadas[oleadaActual].enemiesDead++;
		GameManager.instance.UpdateEnemies(oleadaActual, oleadas.Length, oleadas[oleadaActual].enemiesDead, oleadas[oleadaActual].enemies.Length);
        yield return new WaitForSeconds(tiempoEntreRondas);
        if (oleadas[oleadaActual].enemiesDead >= oleadas[oleadaActual].enemies.Length && oleadaActual<oleadas.Length-1) {
            oleadaActual++;
            shouldSpawn = true;
			GameManager.instance.UpdateEnemies(oleadaActual, oleadas.Length, oleadas[oleadaActual].enemiesDead, oleadas[oleadaActual].enemies.Length);
		}
	}
}

