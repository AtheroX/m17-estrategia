﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBullet : MonoBehaviour{

	public float movSpeed = 40f;
	public int damage = 1;

	Transform target;
    ParticleSystem ps;

    /// <summary>
    /// Guarda el target
    /// </summary>
    /// <param name="target">Target a seguir</param>
	public void Seek(Transform target) {
		this.target = target;
        ps = transform.GetChild(0).GetComponent<ParticleSystem>();
    }

    public void Update() {
        //Si no hay un target se destruye
		if (target == null) {
			Destroy(this.gameObject);
			return;
		}

        //Calcula la dirección afectandole el offset
		Vector3 dir = target.position + target.GetComponent<Enemy>().offsetToShoot - transform.position;
		float deltaDistance = movSpeed * Time.deltaTime;
        //Si al moverse llega al objetivo hace Hit() (De esta forma nos quitamos colliders)
		if (dir.magnitude <= deltaDistance) {
			Hit();
			return;
		}

        //Se mueve ese vector
		transform.Translate(dir.normalized * deltaDistance, Space.World);
	}

    /// <summary>
    /// Si el target sigue existiendo, le daña, se esconde la bala, ejecuta sus particulas y se destruye
    /// </summary>
	private void Hit() {
        if (target != null) {
            target.GetComponent<Enemy>().Hit(damage);
            GetComponent<SpriteRenderer>().enabled = false;
			ps.transform.parent = transform.parent;
            ps.Play();
            Destroy(this.gameObject);
			Destroy(ps.gameObject, 0.3f);
			
        } else 
		    Destroy(this.gameObject);
	}
}
