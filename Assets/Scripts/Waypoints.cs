﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour{

    [HideInInspector] public Transform[] points;
    public Material mat;
	[HideInInspector] public float lineWidth;

    //Coger los waypoints y ponerlos en orden
    void Awake() {
        points = new Transform[transform.childCount];
        
        /*Pone el ancho que se usará para el punto 1 del actual y después del primer punto será el 2o 
        * punto y el siguiente 1 */
        float width = 0.30f+Random.Range(0, .15f);
        for (int i = 0; i < points.Length; i++) {
            points[i] = transform.GetChild(i);
            if (i < points.Length - 1) {
				this.lineWidth = width;
                LineRenderer lr = transform.GetChild(i).gameObject.AddComponent<LineRenderer>();
                lr.SetPosition(0, transform.GetChild(i).position);
                lr.SetPosition(1, transform.GetChild(i+1).position);
                lr.material = mat;

                lr.startWidth = width;
                width = 0.30f + Random.Range(0, .15f);
                lr.endWidth = width;

                lr.startColor = new Color(1,1,1);
                lr.endColor = new Color(1,1,1);

                //lr.sortingLayerName = "Foreground";
                lr.sortingOrder = 1;
                lr.numCapVertices = 5;


				//Colliders en trigger para que no pueda construir en los caminos
                PolygonCollider2D pc = transform.GetChild(i).gameObject.AddComponent<PolygonCollider2D>();
				pc.isTrigger = true;
				Vector2 secondPos = transform.GetChild(i + 1).position - transform.GetChild(i).position;
				Vector2[] pos = {
					new Vector2(0,0-this.lineWidth*1.5f),
					new Vector2(secondPos.x,secondPos.y-width*1.5f),
					new Vector2(secondPos.x,secondPos.y+width*1.5f),
					new Vector2(0,0+this.lineWidth*1.5f)
				};
				pc.SetPath(0, pos);

				//Collider circular de los waypoints
				CircleCollider2D cc = transform.GetChild(i).gameObject.AddComponent<CircleCollider2D>();
				cc.isTrigger = true;
				cc.radius = this.lineWidth * 1.5f;
				cc.offset = Vector2.zero;
			}
        }
    }
    
}
