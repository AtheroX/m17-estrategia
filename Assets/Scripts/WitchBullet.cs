﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WitchBullet : MonoBehaviour{

    int damage = 1;
    ParticleSystem ps;

	/// <summary>
	/// Pone el daño a la bala (porque iban a haber mejoras que suben daño)
	/// </summary>
	/// <param name="i"></param>
    public void SetDamage(int i) {
        damage = i;
        ps = transform.GetChild(0).GetComponent<ParticleSystem>();
    }

	/// <summary>
	/// Si colisiona con enemigo, ejecuta particulas, se detruye la bala y después se detruyen las partículas
	/// </summary>
	/// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.transform.CompareTag("Enemy")) {
            ps.transform.parent = transform.parent;
            ps.Play();
            other.GetComponent<Enemy>().Hit(damage);
            Destroy(this.gameObject);
			Destroy(ps.gameObject, 0.3f);
        }
    }
}
