﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pool: MonoBehaviour {


	/// <summary>
	/// IGNORA ESTE INTENTO, NO DIO TIEMPO
	/// </summary>



    Transform parent;
    public GameObject baseItem;
    public int quant;
    public Item[] itemPool;


    //private void Start() {
    //    itemPool = new Item[quant];
    //    foreach (Item i in itemPool) {
    //        GameObject a = (GameObject)Instantiate(baseItem, transform);
    //        a.transform.parent = parent; //Disposable
    //        i.SetItem(a);
    //        i.GetItem().transform.position = new Vector3(0, 0, -20);
    //        i.GetItem().SetActive(false);
    //    }
    //}

    public GameObject GiveMeItem() {
        foreach (Item item in itemPool) {
            if (!item.GetBeingUsed()) {
                item.SetBeingUsed(true);
                item.GetItem().SetActive(true);
                return item.GetItem();
            }
        }
        return null;
    }

    public void ReturnItem(GameObject returned) {
        returned.transform.position = new Vector3(0, 0, -20);
        foreach (Item item in itemPool) {
            if (item.GetBeingUsed()) {
                item.SetBeingUsed(false);
                item.SetItem(returned);
                item.GetItem().SetActive(false);
            }
        }
    }

    [System.Serializable]
    public class Item {
        GameObject itemGo;
        bool beingUsed = false;

        public GameObject GetItem() {
            return itemGo;
        }

        public void SetItem(GameObject itemGo) {
            this.itemGo = itemGo;
        }

        public bool GetBeingUsed() {
            return beingUsed;
        }

        public void SetBeingUsed(bool beingUsed) {
            this.beingUsed = beingUsed;
        }
    }

}
