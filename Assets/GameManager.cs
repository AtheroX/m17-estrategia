﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour{

	public static GameManager instance;

	public Slider slidPlayerHealth;
	public Slider slidChestSpace;
	public TextMeshProUGUI txtMoney;
	public TextMeshProUGUI txtEnemies;
	public int maxChestSpace = 20;
	public int chestSpace;
	public GameObject player;

	public GameObject victoryPanel;
	public GameObject deathPanel;

	[HideInInspector] public bool playing = false;

	public int money = 200;

	void Awake(){
		//Singleton
		if (instance == null) {
			instance = this;
		} else {
			instance.OnLoadNewLevel();
			Destroy(this.gameObject);
		}

		//CreatePlayer();
		chestSpace = maxChestSpace;
		slidChestSpace.interactable = false;
		slidPlayerHealth.interactable = false;

		playing = true;
		victoryPanel.SetActive(false);
		deathPanel.SetActive(false);
	}

	public void Update() {
		//Skipear nivel
		if (Input.GetKeyDown(KeyCode.C)) {
			UpdateEnemies(1, 1, 1, 1);
		}
	}

	/// <summary>
	/// Función que llena el cofre y mueve el slider 
	/// </summary>
	/// <param name="space">Espacio del cofre a llenar</param>
	public void ChestChange(int space) {
		chestSpace -= space;
		slidChestSpace.value = 1f/((float)maxChestSpace / (float)chestSpace);
		if (chestSpace <= 0) {
			playing = false;
			deathPanel.SetActive(true);
		}
	}

	/// <summary>
	/// Cambia la barra de vida
	/// </summary>
	/// <param name="maxHealth">Vida máxima del jugador</param>
	/// <param name="actualHealth">Vida actual del jugador</param>
	public void HealthChange(int maxHealth, int actualHealth) {
		slidPlayerHealth.value = 1 / ((float)maxHealth / (float)actualHealth);
	}

	/// <summary>
	/// Cambia el dinero y el texto de dinero
	/// </summary>
	/// <param name="money">Cambio de dinero, puede ser negativo</param>
	public void ModifyMoney(int money) {
		this.money += money;
		txtMoney.text = this.money+"";
	}

	/// <summary>
	/// Coge al player de la escena
	/// </summary>
	/// <returns></returns>
	void OnLoadNewLevel() {
		player = FindObjectOfType<Player>().gameObject;
	}

	/// <summary>
	/// Cambia el texto de enemigos restantes y en caso de que sea la última 
	/// </summary>
	/// <param name="actualRound"></param>
	/// <param name="maxRounds"></param>
	/// <param name="enemies">Número de enemigos muertos en esta ronda</param>
	/// <param name="maxEnemies">Número de enemigos máximos en esta ronda</param>
	public void UpdateEnemies(int actualRound, int maxRounds, int enemies, int maxEnemies) {
		if (actualRound + 1 >= maxRounds && playing) {
			playing = false;
			victoryPanel.SetActive(true);
			PlayerPrefs.SetInt("wonLevels", SceneManager.GetActiveScene().buildIndex);
			PlayerPrefs.Save();
		}
		txtEnemies.text = "Oleada: " + (actualRound+1) + "/" + maxRounds + "\n" + enemies + "/" + maxEnemies;
	}
}
