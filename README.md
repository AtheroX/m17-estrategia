Funcionament
El funcionament del joc és agafar el típic TowerDefense i ampliar-ho a un mapa obert, els enemics passaran pel camí per arribar al cofre.
El cofre és un portal a un altre univers, on caben x enemics que volen els consoladors de la dimensió del cofre.
Tu, posant torres pel mapa i amb els teus propis atacs has d'evitar que els enemics arribin al cofre.

A cada Nivell hi ha 15 rondes, amb més enemics per cada ronda, els quals has d'eliminar.

Hi ha 4 Tipus diferents d'Enemics:
- Bàsics: Tenen vida normal, velocitat normal i atac normal.
- Grocs: Tenen menys vida, molta velocitat i atac normal.
- Vermells: Tenen més vida, menys velocitat i més atac.
- Gegants: Tenen molta vida, poca velocitat i molt atac.

Controls
- WASD per al moviment.
- Shift per a córrer.
- Clic dret a on està el ratolí per a disparar, si tens seleccionada una torre i tens monedes la construeix.
- Amb la C pots passar el nivell per a proves.
- Si fas clic dret sobre la torre, surt un menú de venda que si t'allunyes desapareix o al passar x segons.

Risketos Mínims
- Control per mouse. (Clicks, etc.)
- Mapa creat mitjançant tilemap, pla o isomètric, amb diverses capes i col·lisions. (Tilemap, isomètric, te varies capes i col·lisions)
- UI complexa amb diferents elements interactuables. (Sliders, botones, TMP, máscaras de recorte, grupos horizontales y verticales, grids de elementos)
- Gestió de recursos. (Dinero)
- Diverses escenes amb persistència de dades entre les escenes. (PlayerPrefs y Singletons)
- Pas d’informació entre Scripts mitjançant delegats i un sistema d’events. (Action y Lambda)
- Informació per pantalla mitjançant canvas. Ús d’imatges i botons. (El canvas te el HUD amb vida, or, espai del cofre, i les torres)
- Dificultat dinàmica i incremental. (Per cada ronda surten mes enemics i mes forts)

Risketos Adicionales
- Enemics segueixen el camí.
- Animacións. (no moltes)
- Moltes corrutines. 